package com.company;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.io.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
    public static void main(String[] args) throws IOException {
        int quantity;
        int counter;
        JSONParser jsonParser = new JSONParser();
        Object prm = null;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileName = reader.readLine();
        if (!("".equals(fileName))) {
            try {
                prm = jsonParser.parse(new FileReader(fileName));
            } catch (FileNotFoundException e) {
                System.out.println("File not found!");
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }
            reader.close();
            JSONObject obj = (JSONObject) prm;
            quantity = Integer.parseInt(obj.get("quantity").toString());
            counter = Integer.parseInt(obj.get("counter").toString());
            if (quantity > 0 && counter != 0) {
                ExecutorService executorService = Executors.newFixedThreadPool(quantity);
                for (int a = 0; a < quantity; a++) {
                    executorService.execute(() ->
                    {
                        if(counter > 0) {
                            for (int i = 1; i <= counter; i++) {
                                System.out.println("Поток " + Thread.currentThread().getName() + " count: " + i);
                            }
                            System.out.println("Поток " + Thread.currentThread().getName() + " ended!");
                        }
                        else
                        {
                            for(int i = 0; i >= counter;i--){
                                System.out.println("Поток " + Thread.currentThread().getName() + " count: " + i);
                            }
                            System.out.println("Поток " + Thread.currentThread().getName() + " ended!");
                        }
                    });
                }
                executorService.shutdown();
            }
        }
    }
}
